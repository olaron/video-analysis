# Endoscopic workflow analysis

## Description

This is an attempt for the sub-challenge 
[“Surgical Workflow Analysis in the SensorOR” of the "Endoscopic Vision Challenge" 2017
](https://endovissub2017-workflow.grand-challenge.org/)

We used a deep learning technique inspired by the following projects:

* [Activity Recognition with RNN and Temporal-ConvNet](https://github.com/chihyaoma/Activity-Recognition-with-CNN-and-RNN)

* [ResNet training in Torch](https://github.com/facebook/fb.resnet.torch)

### Preprocessing 

First, we crop each frame of each video into smaller images (224x224)

Then, we use them to compute the optical flow like in [Activity Recognition with RNN and Temporal-ConvNet](https://github.com/chihyaoma/Activity-Recognition-with-CNN-and-RNN)

### Processing

We start by training 6 different CNNs using a [pretrained ResNet-34 model](https://github.com/facebook/fb.resnet.torch/tree/master/pretrained):
one for each of the 3 operation types using RGB frames (224x224 at 1 fps), and another 3 using stacks of 10 frames of optical flow (also 224x224 at 1 fps).

For each operation type, we discard all the frames regarding a phase 
that cannot be present for it, and we keep 2 whole videos for testing.

For instance: when we train a network for the sigmoid resection surgery, 
we use all the videos for training except Sigma1 and Sigma7 that will be used for testing.
For all the other videos, we will remove every frame related to the phases 3, 7 and 11 
of our training dataset, because these phases are not present in a sigmoid resection surgery.
We also discard every frame related to the phase 13 (NOOP).

Once the CNNs are trained, we use these models to generate feature vectors of 1024 elements 
that we use as input for 3 different RNNs (one for each operation type).
These 1024 elements are a concatenation of a 512 feature vector from the RGB model, 
and a 512 feature vector from the optical flow model.
This is the same method as in [Activity Recognition with RNN and Temporal-ConvNet](https://github.com/chihyaoma/Activity-Recognition-with-CNN-and-RNN)
except our RNNs are made of a simple LSTM layer followed by a fully connected layer,
in order to get an output for each frame of each video (instead of a single output per video).

## Requirements

[Torch7](http://torch.ch/docs/getting-started.html#_)
(with [Video-decoder](https://github.com/e-lab/torch-toolbox/tree/master/Video-decoder), 
      [csvigo](https://github.com/clementfarabet/lua---csv), 
      [torchzlib](https://github.com/jonathantompson/torchzlib), 
      [pastalog](https://github.com/rewonc/pastalog))

[CUDA 8](http://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#axzz4r9I7WwQ6)

CuDNN 5.1

ffmpeg

OpenCV 3.2

Tested on Ubuntu 16.04 LTS

## How it works

Assuming the videos are present in the directory: `~/Endovis/RGB/`

Assuming the annotations are present in the directory: `~/Endovis/Annotations/`

* ### Step 1: Transforming videos into cropped frames

```bash
th video2croppedframes.lua -dS ~/Endovis/RGB/ -dO ~/Endovis/RGB-crop-frame/
```

* ### Step 2
  The 2 following steps can be done in parallel

  * ### Step 2.1: Get the optical flow

    * ##### Step 2.1.1: Transforming cropped frames into cropped videos

        ```bash
        cd Optical-flow
        ./frames2video.sh -i ~/Endovis/RGB-crop-frame/ -o ~/Endovis/RGB-crop/
        ```

    * ##### Step 2.1.2: Transforming cropped RGB videos into cropped optical flow videos

        See `Optical-flow` folder for instructions

    * ##### Step 2.1.3: Transforming cropped optical flow videos to cropped frames in respect to their different classes
        
        ```bash
        th frames2classes.lua -dS ~/Endovis/Flow-crop/ -dO ~/Endovis/Flow-crop-frame/ -dA ~/Endovis/Annotations/ -v -f
        ```

  * #### Step 2.2: Rearranging RGB frames in respect to their classes

    ```bash
    th frames2classes.lua -dS ~/Endovis/RGB-crop-frame/ -dO ~/Endovis/RGB-crop-frame-classes/ -dA ~/Endovis/Annotations/
    ```

* ### Step 3: Train the CNNs
  The 2 following steps can be done in parallel

  * #### Step 3.1: Train the RGB CNN for each operation type
 
    ```bash
    cd fb.resnet.torch
    th main.lua -nEpochs 30 -LR 0.001 -batchSize 100 -depth 34 -dropout 0.2 -opType Prokto -retrain "models/resnet-34.t7" -name EndoVis-RGB-100-0.001-34-Dropout_0.2-Prokto -data ~/Endovis/RGB-crop-frame-classes/ -save ~/Endovis/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Prokto/ -resume ~/Endovis/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Prokto/
    th main.lua -nEpochs 30 -LR 0.001 -batchSize 100 -depth 34 -dropout 0.2 -opType Rektum -retrain "models/resnet-34.t7" -name EndoVis-RGB-100-0.001-34-Dropout_0.2-Rektum -data ~/Endovis/RGB-crop-frame-classes/ -save ~/Endovis/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Rektum/ -resume ~/Endovis/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Rektum/
    th main.lua -nEpochs 30 -LR 0.001 -batchSize 100 -depth 34 -dropout 0.2 -opType Sigma -retrain "models/resnet-34.t7" -name EndoVis-RGB-100-0.001-34-Dropout_0.2-Sigma -data ~/Endovis/RGB-crop-frame-classes/ -save ~/Endovis/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Sigma/ -resume ~/Endovis/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Sigma/
    ```
    
  * #### Step 3.2: Train the optical flow CNN for each operation type
  
    ```bash
    cd fb.resnet.torch
    th main.lua -nEpochs 30 -LR 0.01 -batchSize 100 -depth 34 -dropout 0.2 -opType Prokto -retrain "models/resnet-34.t7" -dataset endovis-flow -name EndoVis-Flow-100-0.01-34-Dropout_0.2-Prokto -data ~/Endovis/Flow-crop-frame-classes/ -save ~/Endovis/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Prokto/ -resume ~/Endovis/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Prokto/
    th main.lua -nEpochs 30 -LR 0.01 -batchSize 100 -depth 34 -dropout 0.2 -opType Rektum -retrain "models/resnet-34.t7" -dataset endovis-flow -name EndoVis-Flow-100-0.01-34-Dropout_0.2-Rektum -data ~/Endovis/Flow-crop-frame-classes/ -save ~/Endovis/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Rektum/ -resume ~/Endovis/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Rektum/
    th main.lua -nEpochs 30 -LR 0.01 -batchSize 100 -depth 34 -dropout 0.2 -opType Sigma -retrain "models/resnet-34.t7" -dataset endovis-flow -name EndoVis-Flow-100-0.01-34-Dropout_0.2-Sigma -data ~/Endovis/Flow-crop-frame-classes/ -save ~/Endovis/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Sigma/ -resume ~/Endovis/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Sigma/
    ```
    
* ### Step 4: Generate features from the 2 stream CNN for each operation type
   
    ```bash
    cd CNN-features
    qlua twoStreams.lua -sVP ~/Endovis/RGB-crop/ -tVP ~/Endovis/Flow-crop/ -sMP ~/Endovis/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Prokto/model_best.t7 -tMP ~/Endovis/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Prokto/model_best.t7 -dA ~/Endovis/Annotations/ -oP ~/Endovis/Features-Dropout_0.2-Prokto/
    qlua twoStreams.lua -sVP ~/Endovis/RGB-crop/ -tVP ~/Endovis/Flow-crop/ -sMP ~/Endovis/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Rektum/model_best.t7 -tMP ~/Endovis/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Rektum/model_best.t7 -dA ~/Endovis/Annotations/ -oP ~/Endovis/Features-Dropout_0.2-Rektum/
    qlua twoStreams.lua -sVP ~/Endovis/RGB-crop/ -tVP ~/Endovis/Flow-crop/ -sMP ~/Endovis/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Sigma/model_best.t7 -tMP ~/Endovis/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Sigma/model_best.t7 -dA ~/Endovis/Annotations/ -oP ~/Endovis/Features-Dropout_0.2-Sigma/
    ```
    
* ### Step 5: Train the RNN for each operation type

    ```bash
    cd RNN
    th main.lua -pastalogName EndoVis-RNN-0.00001-Dropout_0.2-Prokto -opType Prokto -learningRate 0.00001 -featDir ~/Endovis/Features-Dropout_0.2-Prokto/ -logPath  ~/Endovis/Checkpoints/checkpoints-EndoVis-RNN-0.00001-Dropout_0.2-Prokto/
    th main.lua -pastalogName EndoVis-RNN-0.00001-Dropout_0.2-Rektum -opType Rektum -learningRate 0.00001 -featDir ~/Endovis/Features-Dropout_0.2-Rektum/ -logPath  ~/Endovis/Checkpoints/checkpoints-EndoVis-RNN-0.00001-Dropout_0.2-Rektum/
    th main.lua -pastalogName EndoVis-RNN-0.00001-Dropout_0.2-Sigma -opType Sigma -learningRate 0.00001 -featDir ~/Endovis/Features-Dropout_0.2-Sigma/ -logPath  ~/Endovis/Checkpoints/checkpoints-EndoVis-RNN-0.00001-Dropout_0.2-Sigma/
    ```

* ### Step 6: Use the trained RNN and the features from the CNN to generate the predictions for the testing dataset

    ```bash
    cd Prediction
    th RNN_prediction.lua -MP "~/Endovis/Checkpoints/checkpoints-EndoVis-RNN-0.0001-Dropout_0.2-Prokto-Dropout_0.8/log_EndoVis-RNN-0.0001-Dropout_0.2-Prokto-Dropout_0.8_{0}_{512}_0.0001_0_0.8/model_best.t7" -featDir "~/Endovis/Features-Dropout_0.2-Prokto/" -save "~/Endovis/PhasePredictions/" -opType Prokto -avg 10 -split test
    th RNN_prediction.lua -MP "~/Endovis/Checkpoints/checkpoints-EndoVis-RNN-0.0001-Dropout_0.2-Rektum-Dropout_0.8/log_EndoVis-RNN-0.0001-Dropout_0.2-Rektum-Dropout_0.8_{0}_{512}_0.0001_0_0.8/model_best.t7" -featDir "~/Endovis/Features-Dropout_0.2-Rektum/" -save "~/Endovis/PhasePredictions/" -opType Rektum -avg 10 -split test
    th RNN_prediction.lua -MP "~/Endovis/Checkpoints/checkpoints-EndoVis-RNN-0.0001-Dropout_0.2-Sigma-Dropout_0.8/log_EndoVis-RNN-0.0001-Dropout_0.2-Sigma-Dropout_0.8_{0}_{512}_0.0001_0_0.8/model_best.t7" -featDir "~/Endovis/Features-Dropout_0.2-Sigma/" -save "~/Endovis/PhasePredictions/" -opType Sigma -avg 10 -split test
    ```