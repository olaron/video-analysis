#!/usr/bin/env bash

#Get the parameters
while test $# -gt 0; do
    case "$1" in
        -h|--help)
            echo "options:"
            echo "-h, --help                show brief help"
            echo "-i, --inputPath=INPUT_PATH       specify the path containing all the frames"
            echo "-o, --outputPath=OUTPUT_PATH       specify the path of the output video"
            exit 0
            ;;
        -i)
            shift
            if test $# -gt 0; then
                export INPUT_PATH=$1
            else
                echo "no input path specified"
                exit 1
            fi
            shift
            ;;
        --inputPath*)
            export INPUT_PATH=`echo $1 | sed -e 's/^[^=]*=//g'`
            shift
            ;;
        -o)
            shift
            if test $# -gt 0; then
                export OUTPUT_PATH=$1
            else
                echo "no output path specified"
                exit 1
            fi
            shift
            ;;
        --outputPath*)
            export OUTPUT_PATH=`echo $1 | sed -e 's/^[^=]*=//g'`
            shift
            ;;
        *)
            break
            ;;
    esac
done

function run_conversion {
    local v=${1::-1}
    ffmpeg -r 25 -f image2 -s 224x224 -i "${INPUT_PATH}/${v}/${v}_%d.png" -vcodec libx264 -crf 0 -preset ultrafast -pix_fmt yuv420p "${OUTPUT_PATH}/${v}.mp4"
}

mkdir -p "${OUTPUT_PATH}"

cd "${INPUT_PATH}"
for VIDEO_NAME in */ ; do
    echo ${VIDEO_NAME}
    run_conversion ${VIDEO_NAME}
done
