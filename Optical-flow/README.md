#Optical flow

This compute the TV-L1 optical flow of a set of videos contained in a folder

##Dependencies

g++
pkg-config
OpenCV

##Build

```bash
g++ optical_flow.cpp -o optical_flow `pkg-config --cflags --libs opencv`
```

##Run

```bash
./optical_flow ~/Endovis/RGB-crop/ ~/Endovis/Flow-crop/
```