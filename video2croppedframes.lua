-- Load all the videos & Extract all the frames for these videos

require 'torch'
require 'image'

local videoDecoder = assert(require("libvideo_decoder")) -- package 3


function image_crop(img,height,width)

    local img_2 = img[1]+img[2]+img[3]
    local cX = width/2
    local cY = height/2
    local d = height/2 - 1

    if false then
        local Xmin = width
        local Xmax = 0
        local Ymin = height
        local Ymax = 0
        local nothing = true

        for y = 1,height do 
        for x = 1,width do 
            if img_2[y][x]>0.5 then
                nothing = false
                Ymax = math.max(Ymax, y)
                Ymin = math.min(Ymin, y)
                Xmax = math.max(Xmax, x)
                Xmin = math.min(Xmin, x)
            end
        end
        end

        if nothing then
            Xmin = 0
            Xmax = width
            Ymin = 0
            Ymax = height
        end

        local dX = (Xmax - Xmin)
        local dY = (Ymax - Ymin)  
        cX = Xmin + dX/2
        cY = Ymin + dY/2
        d = math.min(dX,dY)/2
    else
        local img_2 = torch.sign((img_2/3)-0.1)+1
        local s = img_2:size()
        local mass = torch.sum(img_2)
        if mass ~= 0 then
            cY = (torch.sum(img_2,2):squeeze() * torch.range(1,s[1])) / mass
            cX = (torch.sum(img_2,1):squeeze() * torch.range(1,s[2])) / mass
            d = math.min(cX,cY,width-cX,height-cY)
        end  
    end

    local Xmin_2 = cX - d
    local Xmax_2 = cX + d
    local Ymin_2 = cY - d
    local Ymax_2 = cY + d

    if opt.drawRect then
        --img = image.drawRect(img,Xmin,Ymin,Xmax,Ymax)
        img = image.drawRect(img,cX,cY,cX+1,cY+1)
        img = image.drawRect(img,Xmin_2,Ymin_2,Xmax_2,Ymax_2)
        img = image.toDisplayTensor({input=img})
    else 
        img = image.crop(img,Xmin_2,Ymin_2,Xmax_2,Ymax_2)
    --TODO option scale dimension
        img = image.scale(img,224,224,'bicubic')
    end

    return img
  
end


----------------
-- parse args --
----------------
op = xlua.OptionParser('%prog [options]')

op:option{'-fS', '--fileSource', action='store', dest='fileSource',help='video file path', default=nil}
op:option{'-dS', '--dirSource', action='store', dest='dirSource',help='video directory path', default=nil}
op:option{'-dO', '--dirOutput', action='store', dest='dirOutput',help='output directory', default=''}
op:option{'-f', '--fps', action='store', dest='fps',help='number of frames per second', default=25}
op:option{'-tH', '--threads', action='store', dest='threads', help='number of threads', default=1}
op:option{'-dR', '--drawRectangles', action='store_true', dest='drawRect',help='draw rectangles', default=false }

opt,args = op:parse()

-- convert strings to numbers --
fps = tonumber(opt.fps)
threads = tonumber(opt.threads)

print('threads #: '..threads)
print('fps: '..fps)


----------------------------------
-- 				Data paths				    --
----------------------------------

-- outputs
dirVideoOut = opt.dirOutput


--------------------------------------
--  		       CPU option	 	        --
--------------------------------------

-- nb of threads and fixed seed (for repeatable experiments)
torch.setnumthreads(threads)
torch.manualSeed(1)
torch.setdefaulttensortype('torch.FloatTensor')

-- ----------------------------------------------
-- --         Input/Output information         --
-- ----------------------------------------------

--------------------------------
-- 					Class		        	--
--------------------------------
if opt.dirSource then
    nameVideos=paths.dir(opt.dirSource)
    table.sort(nameVideos) -- ascending order
    table.remove(nameVideos,1) -- remove "."
    table.remove(nameVideos,1) -- remove ".."
elseif opt.fileSource then
    nameVideos={opt.fileSource}
else
   error('No fileSource or dirSource provided')
end

timerAll = torch.Timer() -- count the whole processing time


for sv=1, #nameVideos do
  local timerClass = torch.Timer() -- count the processing time for one class
  
  ----------------------
  ---- Load the video --
  ----------------------  
  local videoName = paths.basename(nameVideos[sv],'avi')
  local videoPath = opt.fileSource or opt.dirSource .. videoName .. '.avi'

  print('Current Video: ' .. videoName)
  
  local status, height, width, length, fps = videoDecoder.init(videoPath)
  
  local pathVideoOut = dirVideoOut .. videoName

  -- save all the frames & count frame numbers
  local inFrame = torch.FloatTensor(3, height, width)
  local countFrame = 0

  if not paths.dirp(pathVideoOut) then
    paths.mkdir(pathVideoOut)
  end
  
  local message = ''

  local t = torch.Timer()
  while videoDecoder.frame_rgb(inFrame) do
   
    local timeSpent = t:time().real
    local eta = (timeSpent / countFrame) * (length - countFrame)
    io.write(string.rep('\b',#message))
    message = 'Frame: ' .. (countFrame) .. '/' .. (length-1) .. ' | ETA: ' .. string.format('%.2f',eta) .. 's    '
    io.write(message)
    io.flush()

    local frameName = videoName .. '_' .. countFrame .. '.png'
    local framePath = paths.concat(pathVideoOut,frameName)
    
    img = image_crop(inFrame,height,width)
    
    image.save(framePath,img)

    countFrame = countFrame + 1
  end
  
  videoDecoder.exit()
  print('')
  print('The elapsed time for the video '..nameVideos[sv]..': ' .. timerClass:time().real .. ' seconds')
			
end

print('The total elapsed time : ' .. timerAll:time().real .. ' seconds')
  
