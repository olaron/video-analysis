require 'csvigo'

print('data-generated')
function get_data()
    local trainFeats = {}
    local trainTargets = {}

    local frameInterval = 25

    for i=1, 20 do
        local obs = csvigo.load({path=paths.concat(opt.featDir,'Observations_s_'..i..'.csv'),mode='large'})
        local phs = csvigo.load({path=paths.concat(opt.featDir,'Phase_s_'..i..'.csv'),mode='large'})
        local feats = {}
        local targets = {}
        for k,v in pairs(obs) do
            local feat = {}
            feat.frameNumber = (i-1)*frameInterval
            feat.feature = torch.Tensor(v)
            feats[k] = feat
            targets[k] = tonumber(phs[k][1])
        end
        trainFeats[i] = feats
        trainTargets[i] = targets
        collectgarbage()
    end

    local testFeats = {}
    local testTargets = {}

    for i=21, 25 do
        local obs = csvigo.load({path=paths.concat(opt.featDir,'Observations_s_'..i..'.csv'),mode='large'})
        local phs = csvigo.load({path=paths.concat(opt.featDir,'Phase_s_'..i..'.csv'),mode='large'})
        local feats = {}
        local targets = {}
        for k,v in pairs(obs) do
            local feat = {}
            feat.frameNumber = (i-1)*frameInterval
            feat.feature = torch.Tensor(v)
            feats[k] = feat
            targets[k] = tonumber(phs[k][1])
        end
        testFeats[i] = feats
        testTargets[i] = targets
        collectgarbage()
    end

    return {
        trainData = trainFeats,
        trainTarget = trainTargets,
        testData = testFeats,
        testTarget = testTargets
    }
end
