require 'csvigo'

function low_high (myTable)
    local lowest, highest

    for k,_ in pairs(myTable) do
        if type(k) == "number" and k % 1 == 0 and k > 0 then -- Assuming mixed (possibly non-integer) keys
            if lowest then
                lowest = math.min(lowest, k)
                highest = math.max(highest, k)
            else
                lowest, highest = k, k
            end
        end
    end

    return lowest or 0, highest or 0 -- "or 0" in case there were no indices
end

function get_data()

    print('data-endovis')

    local trainFeats = {}
    local trainTargets = {}

    local frameInterval = 25

    for _,v in ipairs(splits[opt.opType].train) do
        local featsFile = torch.load(paths.concat(opt.featDir,v..'.t7'))
        local device
        if opt.deviceDataPath ~='none' then
            device = csvigo.load({path=paths.concat(opt.deviceDataPath,v..'_Device.csv'),mode='large'})
        end
        local feats = {}
        local targets = {}
        --print(featsFile.Flow.frame)
        print(v)
        local low_i,_ = low_high(featsFile.Flow.frame)
        for i,w in pairs(featsFile.Flow.frame) do
            local feat = w
            local target = w.label
            feat.output = nil
            feat.frameNumber = i
            if not opt.noFlow then
                feat.feature = torch.cat(featsFile.RGB.frame[i].feature,featsFile.Flow.frame[i].feature,2):squeeze()
            else
                feat.feature = featsFile.RGB.frame[i].feature:squeeze()
            end
            if device then
                local data = device[i]
                table.remove(data,1)
                feat.feature = torch.cat(feat.feature,torch.Tensor(data),1)
            end
            feats[i/frameInterval-low_i/frameInterval+1] = feat
            targets[i/frameInterval-low_i/frameInterval+1] = target
        end
        trainFeats[v] = feats
        trainTargets[v] = targets

        collectgarbage()
    end

    local testFeats = {}
    local testTargets = {}

    for _,v in ipairs(splits[opt.opType].test) do
        local featsFile = torch.load(paths.concat(opt.featDir,v..'.t7'))
        local device
        if opt.deviceDataPath ~='none' then
            device = csvigo.load({path=paths.concat(opt.deviceDataPath,v..'_Device.csv'),mode='large'})
        end
        local feats = {}
        local targets = {}
        --print(featsFile.Flow.frame)
        print(v)
        local low_i,_ = low_high(featsFile.Flow.frame)
        for i,w in pairs(featsFile.Flow.frame) do
            local feat = w
            local target = w.label
            feat.output = nil
            feat.frameNumber = i
            if not opt.noFlow then
                feat.feature = torch.cat(featsFile.RGB.frame[i].feature,featsFile.Flow.frame[i].feature,2):squeeze()
            else
                feat.feature = featsFile.RGB.frame[i].feature:squeeze()
            end
            if device then
                local data = device[i]
                table.remove(data,1)
                feat.feature = torch.cat(feat.feature,torch.Tensor(data),1)
            end
            feats[i/frameInterval-low_i/frameInterval+1] = feat
            targets[i/frameInterval-low_i/frameInterval+1] = target
        end
        testFeats[v] = feats
        testTargets[v] = targets

        collectgarbage()
    end

    return {
        trainData = trainFeats,
        trainTarget = trainTargets,
        testData = testFeats,
        testTarget = testTargets
    }
end