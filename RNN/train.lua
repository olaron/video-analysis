----------------------------------------------------------------
-- Activity-Recognition-with-CNN-and-RNN
-- https://github.com/chihyaoma/Activity-Recognition-with-CNN-and-RNN
--
--
-- This is a testing code for implementing the RNN model with LSTM
-- written by Chih-Yao Ma.
--
-- The code will take feature vectors (from CNN model) from contiguous
-- frames and train against the ground truth, i.e. the labeling of video classes.
--
-- Contact: Chih-Yao Ma at <cyma@gatech.edu>
----------------------------------------------------------------

local sys = require 'sys'
local xlua = require 'xlua' -- xlua provides useful tools, like progress bars
local optim = require 'optim'

print(sys.COLORS.red .. '==> defining some tools')

-- model:
local m = require 'model'
print('model imported')

local model = m.model
local criterion = m.criterion

-- Batch test:
local inputs = torch.Tensor(opt.batchSize, opt.inputSize, opt.rho)

local targets = torch.Tensor(opt.batchSize)

if opt.cuda == true then
    inputs = inputs:cuda()
    targets = targets:cuda()
end

print(sys.COLORS.red .. '==> configuring optimizer')
-- Pass learning rate from command line
optimState = optimState or {
    learningRate = opt.learningRate,
    momentum = opt.momentum,
    weightDecay = opt.weightDecay,
    lrMethod = opt.lrMethod,
    epochUpdateLR = opt.epochUpdateLR,
    learningRateDecay = opt.learningRateDecay,
    lrDecayFactor = opt.lrDecayFactor,
    nesterov = true,
    dampening = 0.0
}

-- Retrieve parameters and gradients:
-- this extracts and flattens all the trainable parameters of the mode
-- into a 1-dim vector
local params, gradParams = model:getParameters()

function train(trainData, trainTarget)

    -- epoch tracker
    epoch = epoch or 1

    local timer = torch.Timer()

    model:training()

    local function feval()
        -- clip gradient element-wise
        gradParams:clamp(-opt.gradClip, opt.gradClip)
        return criterion.output, gradParams
    end

    local top1Sum, top3Sum, lossSum = 0.0, 0.0, 0.0
    local N = 0

    print(sys.COLORS.green .. '==> doing epoch on training data:')
    print("==> online epoch # " .. epoch .. ' [batchSize = ' .. opt.batchSize .. ']')

    -- adjust learning rate
    optimState.learningRate = adjustLR(opt.learningRate, epoch)
    print(sys.COLORS.yellow .. '==> Learning rate is: ' .. optimState.learningRate .. '')

    local idVideo = 0
    for videoName,frames in pairs(trainData) do
        idVideo = idVideo + 1
        print(videoName)
        model:forget()
        for i=1,#frames,opt.seqLength do
            local seqLength = math.min(#frames-i,opt.seqLength)
            local input = torch.Tensor(seqLength,frames[i].feature:size(1)):cuda()
            local target = torch.Tensor(seqLength):cuda()
            for j=i, seqLength+i-1 do
                input[j-i+1] = frames[j].feature
                target[j-i+1] = trainTarget[videoName][j]+1
            end

            local output = model:forward(input)

            output = output:float()

            local loss = criterion:forward(model.output, target)

            model:zeroGradParameters()
            criterion:backward(model.output, target)
            model:backward(input, criterion.gradInput)

            local batchSize = 1

            local top1, top3 = computeScore(output, target)
            top1Sum = top1Sum + top1 * batchSize * seqLength
            top3Sum = top3Sum + top3 * batchSize * seqLength
            lossSum = lossSum + loss * batchSize * seqLength
            N = N + batchSize * seqLength

            optim.adam(feval, params, optimState)

            print(('Epoch: [%d][%d][%d/%d]    Time %.3f  Err %1.4f  top1 %7.3f'):format(epoch,idVideo, i, #frames, timer:time().real, loss, top1))
            
            local dataset_progress = (idVideo-1)/#splits[opt.opType].train
            local video_progress = (i/#frames)
            
            pastalog_send(opt.pastalogName or opt.dataset,today_date,top1, (epoch-1) + dataset_progress + video_progress/#splits[opt.opType].train ,'http://10.100.222.155:8120/data')
            timer:reset()
        end

    end

    epoch = epoch + 1
end

-- TODO: Learning Rate function 
function adjustLR(learningRate, epoch)
    local decayPower = 0
    if optimState.lrMethod == 'manual' then
        decayPower = decayPower
    elseif optimState.lrMethod == 'fixed' then
        decayPower = math.floor((epoch - 1) / optimState.epochUpdateLR)
    end

    return learningRate * math.pow(optimState.lrDecayFactor, decayPower)
end

function computeScore(output, target)

    -- Coputes the top1 and top3 error rate
    local batchSize = output:size(1)

    local _, predictions = output:float():sort(2, true) -- descending

    -- Find which predictions match the target
    local correct = predictions:eq(target:long():view(batchSize, 1):expandAs(output))

    -- Top-1 score
    local top1 = 1.0 - (correct:narrow(2, 1, 1):sum() / batchSize)

    -- Top-3 score, if there are at least 3 classes
    local len = math.min(3, correct:size(2))
    local top3 = 1.0 - (correct:narrow(2, 1, len):sum() / batchSize)

    return top1 * 100, top3 * 100
end

-- Export:
return train
