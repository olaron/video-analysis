return {
    Prokto = {
        all={"Prokto1","Prokto2","Prokto3","Prokto4","Prokto5","Prokto6","Prokto7","Prokto8","Rektum1","Rektum2","Rektum3","Rektum4","Rektum5","Rektum6","Rektum7","Rektum8","Sigma1","Sigma2","Sigma3","Sigma4","Sigma5","Sigma6","Sigma7","Sigma8"},
        train={"Prokto2","Prokto3","Prokto5","Prokto6","Prokto7","Prokto8","Rektum1","Rektum2","Rektum3","Rektum4","Rektum5","Rektum6","Rektum7","Rektum8","Sigma1","Sigma2","Sigma3","Sigma4","Sigma5","Sigma6","Sigma7","Sigma8"},
        test={"Prokto1","Prokto4" }
    },
    Rektum = {
        all={"Prokto1","Prokto2","Prokto3","Prokto4","Prokto5","Prokto6","Prokto7","Prokto8","Rektum1","Rektum2","Rektum3","Rektum4","Rektum5","Rektum6","Rektum7","Rektum8","Sigma1","Sigma2","Sigma3","Sigma4","Sigma5","Sigma6","Sigma7","Sigma8"},
        train={"Prokto1","Prokto2","Prokto3","Prokto4","Prokto5","Prokto6","Prokto7","Prokto8","Rektum1","Rektum2","Rektum3","Rektum4","Rektum5","Rektum7","Sigma1","Sigma2","Sigma3","Sigma4","Sigma5","Sigma6","Sigma7","Sigma8"},
        test={"Rektum6","Rektum8"}
    },
    RektumOnly = {
        all={"Prokto1","Prokto2","Prokto3","Prokto4","Prokto5","Prokto6","Prokto7","Prokto8","Rektum1","Rektum2","Rektum3","Rektum4","Rektum5","Rektum6","Rektum7","Rektum8","Sigma1","Sigma2","Sigma3","Sigma4","Sigma5","Sigma6","Sigma7","Sigma8"},
        train={"Rektum1","Rektum2","Rektum3","Rektum4","Rektum5","Rektum7"},
        test={"Rektum6","Rektum8"}
    },
    Sigma = {
        all={"Prokto1","Prokto2","Prokto3","Prokto4","Prokto5","Prokto6","Prokto7","Prokto8","Rektum1","Rektum2","Rektum3","Rektum4","Rektum5","Rektum6","Rektum7","Rektum8","Sigma1","Sigma2","Sigma3","Sigma4","Sigma5","Sigma6","Sigma7","Sigma8"},
        train={"Prokto1","Prokto2","Prokto3","Prokto4","Prokto5","Prokto6","Prokto7","Prokto8","Rektum1","Rektum2","Rektum3","Rektum4","Rektum5","Rektum6","Rektum7","Rektum8","Sigma2","Sigma3","Sigma4","Sigma5","Sigma6","Sigma8"},
        test={"Sigma1","Sigma7" }
    },
    generated = {
        all={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25},
        train={1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20},
        test={21,22,23,24,25}
    }
}

