----------------------------------------------------------------
--  Activity-Recognition-with-CNN-and-RNN
--  https://github.com/chihyaoma/Activity-Recognition-with-CNN-and-RNN
--
--
--  This is a testing code for implementing the RNN model with LSTM
--  written by Chih-Yao Ma.
--
--  The code will take feature vectors (from CNN model) from contiguous
--  frames and train against the ground truth, i.e. the labeling of video classes.
--
--  Contact: Chih-Yao Ma at <cyma@gatech.edu>
----------------------------------------------------------------
--if IMPORTED_TEST then return end
--IMPORTED_TEST = true

local nn = require 'nn'
local sys = require 'sys'
local xlua = require 'xlua'    -- xlua provides useful tools, like progress bars
local optim = require 'optim'
require 'csvigo'

print(sys.COLORS.red .. '==> defining some tools')
-- model:
local m = require 'model'
local model = m.model
local criterion = m.criterion

-- This matrix records the current confusion across classes
local confusion = optim.ConfusionMatrix(classes)
print('classes')
print(classes)

-- Batch test:
local inputs = torch.Tensor(opt.batchSize, opt.inputSize, opt.rho)
local targets = torch.Tensor(opt.batchSize)


predsFrames = torch.Tensor(opt.batchSize, nClass, opt.rho - opt.numSegment + 1)

local logsoftmax = nn.LogSoftMax()

if opt.cuda == true then
	inputs = inputs:cuda()
	targets = targets:cuda()
	logsoftmax = logsoftmax:cuda()
end

local top1TotalBest = 100

-- test function
function test(testData, testTarget)

	-- local vars
	local timer = torch.Timer()
	local dataTimer = torch.Timer()

	if opt.cuda == true then
		model:cuda()
	end

	-- Sets Dropout layer to have a different behaviour during evaluation.
	model:evaluate()

	-- test over test data
	print(sys.COLORS.red .. '==> testing on test set:')

	if opt.testOnly then
		epoch = 1
	end

	local top1Total, top3Total= 0.0,0.0
    local Ntotal = 0
    
    local idVideo = 0
    for videoName,frames in pairs(testData) do
        idVideo = idVideo +1
        print(videoName)
        local top1Sum, top3Sum, lossSum = 0.0, 0.0, 0.0
	    local N = 0

        model:forget()
        for i=1,#frames,opt.seqLength do

            local seqLength = math.min(#frames-i+1,opt.seqLength)
            local input = torch.Tensor(seqLength,frames[i].feature:size(1)):cuda()
            local target = torch.Tensor(seqLength):cuda()
            for j=i, seqLength+i-1 do
                input[j-i+1] = frames[j].feature
                target[j-i+1] = testTarget[videoName][j]+1
            end

            local output = model:forward(input):float()

            local batchSize = 1

            local top1, top3 = computeScore(output, target)
            top1Sum = top1Sum + top1 * batchSize * seqLength
            top3Sum = top3Sum + top3 * batchSize * seqLength
            N = N + batchSize * seqLength
            
            top1Total = top1Total + top1 * batchSize * seqLength
            top3Total = top3Total + top3 * batchSize * seqLength
            Ntotal = Ntotal + batchSize * seqLength

            print(('Test: [%d][%d][%d/%d] | Time %.3f  top1 %7.3f (%7.3f)  top3 %7.3f (%7.3f)'):format(
                epoch-1, idVideo, i, #frames, timer:time().real, top1, top1Sum / N, top3, top3Sum / N))

            timer:reset()
        end

    end
    print((' Test accuracy: [%d] | top1 %7.3f top3 %7.3f'):format(epoch-1,top1Total / Ntotal,top3Total / Ntotal))
   
	if opt.cuda == true then
		model:cuda()
	end

	timer:reset()
	dataTimer:reset()
	
	local bestModel = false
	if top1Total / Ntotal < top1TotalBest then
	    top1TotalBest = top1Total / Ntotal
	    bestModel = true
	end
	checkpoints.save(epoch-1,model,optimState,bestModel,top1Total)

end


function computeScore(output, target)

   -- Coputes the top1 and top3 error rate
   local batchSize = output:size(1)

   local _ , predictions = output:float():sort(2, true) -- descending

   -- Find which predictions match the target
   local correct = predictions:eq(target:long():view(batchSize, 1):expandAs(output))

   -- Top-1 score
   local top1 = 1.0 - (correct:narrow(2, 1, 1):sum() / batchSize)

   -- Top-3 score, if there are at least 3 classes
   local len = math.min(3, correct:size(2))
   local top3 = 1.0 - (correct:narrow(2, 1, len):sum() / batchSize)

   return top1 * 100, top3 * 100
end

-- Export:
return test
