#!/usr/bin/env bash

set -e

#Get the parameters
while test $# -gt 0; do
    case "$1" in
        -h|--help)
            echo "options:"
            echo "-h, --help                show brief help"
            echo "-i, --inputPath=INPUT_PATH       specify the path containing the Endovis folder"
            echo "-a, --annotationPath=ANNOTATION_PATH       specify the path containing all annotation files"
            exit 0
            ;;
        -i)
            shift
            if test $# -gt 0; then
                export INPUT_PATH=$1
            else
                echo "no input path specified"
                exit 1
            fi
            shift
            ;;
        -a)
            shift
            if test $# -gt 0; then
                export ANNOTATION_PATH=$1
            else
                echo "no annotation path specified"
                exit 1
            fi
            shift
            ;;
        *)
            break
            ;;
    esac
done

INPUT_PATH=`readlink -f ${INPUT_PATH}`
ANNOTATION_PATH=`readlink -f ${ANNOTATION_PATH}`

SCRIPT_DIR=$(dirname $(readlink -f $0))

cd "${SCRIPT_DIR}"

if [ ! -f Optical-flow/optical_flow ]; then
    echo "Compilation Optical_flow.cpp"
    cd Optical-flow
    g++ optical_flow.cpp -o optical_flow `pkg-config --cflags --libs opencv`
   cd ..
fi

echo "Transforming videos into cropped frames"

th video2croppedframes.lua -dS "${INPUT_PATH}/RGB/" -dO "${INPUT_PATH}/RGB-crop-frame/"


echo "Transforming cropped frames into cropped videos"
./frames2video.sh -i "${INPUT_PATH}/RGB-crop-frame/" -o "${INPUT_PATH}/RGB-crop/"


echo "Rearranging RGB frames in respect to their classes"
th frames2classes.lua -dS "${INPUT_PATH}/RGB-crop-frame/" -dO "${INPUT_PATH}/RGB-crop-frame-classes/" -dA "${ANNOTATION_PATH}"


echo "Transforming cropped RGB videos into cropped optical flow videos"
cd Optical-flow
./optical_flow "${INPUT_PATH}/RGB-crop/" "${INPUT_PATH}/Flow-crop/"
cd "${SCRIPT_DIR}"

echo "Transforming cropped optical flow videos to cropped frames in respect to their different classes"
th frames2classes.lua -dS "${INPUT_PATH}/Flow-crop/" -dO "${INPUT_PATH}/Flow-crop-frame/" -dA "${ANNOTATION_PATH}" -v -f
