return {
    Prokto = {
        all={"Prokto1","Prokto2","Prokto3","Prokto4","Prokto5","Prokto6","Prokto7","Prokto8","Rektum1","Rektum2","Rektum3","Rektum4","Rektum5","Rektum6","Rektum7","Rektum8","Sigma1","Sigma2","Sigma3","Sigma4","Sigma5","Sigma6","Sigma7","Sigma8"},
        train={"Prokto2","Prokto3","Prokto5","Prokto6","Prokto7","Prokto8","Rektum1","Rektum2","Rektum3","Rektum4","Rektum5","Rektum6","Rektum7","Rektum8","Sigma1","Sigma2","Sigma3","Sigma4","Sigma5","Sigma6","Sigma7","Sigma8"},
        val={"Prokto1","Prokto4" },
        test={"Prokto9","Prokto10"}
    },
    Rektum = {
        all={"Prokto1","Prokto2","Prokto3","Prokto4","Prokto5","Prokto6","Prokto7","Prokto8","Rektum1","Rektum2","Rektum3","Rektum4","Rektum5","Rektum6","Rektum7","Rektum8","Sigma1","Sigma2","Sigma3","Sigma4","Sigma5","Sigma6","Sigma7","Sigma8"},
        train={"Prokto1","Prokto2","Prokto3","Prokto4","Prokto5","Prokto6","Prokto7","Prokto8","Rektum1","Rektum2","Rektum3","Rektum4","Rektum5","Rektum7","Sigma1","Sigma2","Sigma3","Sigma4","Sigma5","Sigma6","Sigma7","Sigma8"},
        val={"Rektum6","Rektum8" },
        test={"Rektum9","Rektum10"}

    },
    Sigma = {
        all={"Prokto1","Prokto2","Prokto3","Prokto4","Prokto5","Prokto6","Prokto7","Prokto8","Rektum1","Rektum2","Rektum3","Rektum4","Rektum5","Rektum6","Rektum7","Rektum8","Sigma1","Sigma2","Sigma3","Sigma4","Sigma5","Sigma6","Sigma7","Sigma8"},
        train={"Prokto1","Prokto2","Prokto3","Prokto4","Prokto5","Prokto6","Prokto7","Prokto8","Rektum1","Rektum2","Rektum3","Rektum4","Rektum5","Rektum6","Rektum7","Rektum8","Sigma2","Sigma3","Sigma4","Sigma5","Sigma6","Sigma8"},
        val={"Sigma1","Sigma7" },
        test={"Sigma9","Sigma10"}
    }
}

