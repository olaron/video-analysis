
require 'xlua'
require 'torch'
require 'image'
require 'rnn'
require 'cudnn'
require 'cunn'
require 'cutorch'
require 'csvigo'
optim = require 'optim'

local videoDecoder = assert(require("libvideo_decoder")) -- package 3

----------------
-- parse args --
----------------
op = xlua.OptionParser('%prog [options]')

op:option { '-sVP', '--spatVideoPath', action = 'store', dest = 'spatVideoPath', help = 'spatial video path', default = 'none' }
op:option { '-tVP', '--tempVideoPath', action = 'store', dest = 'tempVideoPath', help = 'temporal video path', default = 'none' }

op:option { '-sMP', '--spatModelPath', action = 'store', dest = 'spatModelPath', help = 'spatial model path', default = 'none' }
op:option { '-tMP', '--tempModelPath', action = 'store', dest = 'tempModelPath', help = 'temporal model path', default = 'none' }

op:option { '-dA', '--dirAnnotation', action = 'store', dest = 'dirAnnotation', help = 'annotation directory path', default = '' }

op:option { '-oP', '--outputPath', action = 'store', dest = 'outputPath', help = 'output path', default = '' }

op:option { '-tO', '--testOnly', action = 'store_true', dest = 'testOnly', help = 'test only', default = false }

op:option { '-fI', '--frameInterval', action = 'store', dest = 'frameInterval', help = 'number of frames used for prediction ', default = 25 }

op:option { '-tH', '--threads', action = 'store', dest = 'threads', help = 'number of threads', default = 1 }
op:option { '-i1', '--devid1', action = 'store', dest = 'devid1', help = '1st GPU', default = 1 }
op:option { '-i2', '--devid2', action = 'store', dest = 'devid2', help = '2nd GPU', default = 1 }

op:option{'-nS', '--nStack', action='store', dest='nStack', help='number of stacked images per frame', default= 10 }

op:option{'-off', '--offset', action='store', dest='offset', help='offset of frames to select', default= 0 }

op:option{'-opType', '--opType' , action = 'store', dest = 'opType', help='Operation type (EndoVis): Prokto | Rektum | Sigma', default = 'none'}


local numStream = 2 -- stream number of the input data
opt, args = op:parse()

assert(opt.opType ~= 'none', '-opType not specified.')


local offset = tonumber(opt.offset)
local frameInterval = tonumber(opt.frameInterval)
local devid1 = tonumber(opt.devid1)
local devid2 = tonumber(opt.devid2)
local threads = tonumber(opt.threads)
local nStack = tonumber(opt.nStack)

local nChannels = {
    3,
    nStack*2
}

-----------------------------------
--          Data paths		     --
-----------------------------------

local DIR = {}

print(opt.spatVideoPath)
----- RGB ----
table.insert(DIR, { dirModel = opt.spatModelPath, pathVideoIn = opt.spatVideoPath })

----- Flow ----
table.insert(DIR, { dirModel = opt.tempModelPath, pathVideoIn = opt.tempVideoPath })


------------------------------
--       CPU option	 	    --
------------------------------

-- nb of threads and fixed seed (for repeatable experiments)
torch.setnumthreads(threads)
torch.manualSeed(1)
torch.setdefaulttensortype('torch.FloatTensor')

--------------------------------------
--    Train/Test split (EndoVis)    --
--------------------------------------

local splits = require './endovis-splits'

if opt.testOnly then
    splits = splits[opt.opType].test
else
    splits = splits[opt.opType].all
end

------------------------------
--     Model Selection      --
------------------------------

local modelPath = {}
for nS = 1, numStream do
    table.insert(modelPath, DIR[nS].dirModel)
end

------------------------------
--          Models          --
------------------------------

local devID = {}
devID[1] = devid2 -- for spatial
devID[2] = devid1 -- for temporal

local net = {}

for nS = 1, numStream do

    --- choose GPU ---
    print('GPU ' .. devID[nS])
    cutorch.setDevice(devID[nS])
    print(sys.COLORS.red .. '==> using GPU #' .. cutorch.getDevice())
    print(sys.COLORS.white .. ' ')

    print(' ')
    if nS == 1 then
        print('==> Loading the spatial model...')
    else
        print('==> Loading the temporal model...')
    end
    print(modelPath[nS])
    local netTemp = torch.load(modelPath[nS]):cuda() -- Torch model

    netTemp:evaluate() -- Evaluate mode
    --print(netTemp)
    table.insert(net, netTemp)

    print(' ')

end

--====================================================================--
--                     Run all the videos in EndoVis                  --
--====================================================================--

print('==> Processing all the videos...')


local timerAll = torch.Timer() -- count the whole processing time

if not paths.dirp(opt.outputPath) then
    paths.mkdir(opt.outputPath)
end

for sv = 1, #splits do

    local feat = {}
    --------------------
    -- Load the video --
    --------------------
    local videoName = splits[sv]
    local videoPath = {}
    print('videoName '.. videoName)
    for nS = 1, numStream do
        local videoPathTemp = DIR[nS].pathVideoIn .. videoName .. '.mp4'
        table.insert(videoPath, videoPathTemp)
    end

    feat.RGB = {videoPath = videoPath[1], frame = {} }
    feat.Flow = {videoPath = videoPath[2], frame = {} }
    feat.videoName = videoName

    local csv
    if not opt.testOnly then
        local csvPath = paths.concat(opt.dirAnnotation,videoName..'.csv')
        csv = csvigo.load({path=csvPath,mode='large'})
    end
    --=========================================--
    --            Process the video            --
    --=========================================--

    --== Read the video ==--
    local message = ''
    for nS = 1, numStream do
        --- - read the video to a tensor ----
        print('Reading the video ' .. videoPath[nS])
        local status, height, width, length, fps = videoDecoder.init(videoPath[nS])

        if nS == 1 then feat.length = length end

        local vidTensorTemp
        local inFrame = torch.ByteTensor(3, height, width)
        local countFrame = -1
        local timer = torch.Timer()

        local confusion = optim.ConfusionMatrix(14,{'0','1','2','3','4','5','6','7','8','9','10','11','12','13'})
        local imgCols = {}
        local queue = {}
        while videoDecoder.frame_rgb(inFrame) do
            local frame = inFrame
            countFrame = countFrame + 1
            if nS == 2 and countFrame % frameInterval == offset then
                frame = inFrame[{{2,3}}]
                table.insert(queue,#queue+1,frame)
            end
            if countFrame % frameInterval == offset and (#queue == nStack or nS == 1) then
                if nS == 2 then
                    frame = torch.cat(queue,1)
                end
                local timeSpent = timer:time().real
                local eta = (timeSpent / (countFrame+1)) * ((length) - (countFrame+1))
                io.write(string.rep('\b',#message+4))
                message = string.format('[%s](%s) Frame: %7d/%d | ETA: %7.2fm',videoName,nS == 1 and "RGB" or "Flow",countFrame,length-1,eta/60)
                io.write(message)
                io.flush()


                local frameTensor = torch.reshape(frame, 1, nChannels[nS], 224, 224):double():div(255)
                local output = net[nS]:forward(frameTensor:cuda())
                output = output:squeeze()

                local labelCol = torch.zeros(14)
                local label
                if not opt.testOnly then
                    label = tonumber(csv[countFrame+1][2])
                    labelCol[label+1]=1
                end

                local _, pred = torch.max(output,1)
                pred=pred[1]
                local outputPred = torch.zeros(14)
                outputPred[pred]=1

                local col = torch.cat({outputPred:float(),torch.zeros(14):float(),labelCol:float()},2)
                imgCols[#imgCols+1]= col:transpose(1,2)

                local feat_now = net[nS].modules[11].output:double()
                if not opt.testOnly then
                    confusion:add(output,label+1)
                    if nS == 1 then
                        feat.RGB.frame[countFrame] = {label = label, feature = feat_now, output = output }
                    elseif nS == 2 then
                        feat.Flow.frame[countFrame] = {label = label, feature = feat_now, output = output }
                    end
                else
                    if nS == 1 then
                        feat.RGB.frame[countFrame] = {feature = feat_now, output = output }
                    elseif nS == 2 then
                        feat.Flow.frame[countFrame] = {feature = feat_now, output = output }
                    end

                end
            end
            if nS == 2 and #queue == nStack then
                table.remove(queue,1)
            end

        end

        if not opt.testOnly then
            confusion:updateValids()
            print('')
            print(confusion)
            print('')
            image.save(opt.outputPath..videoName..'_Confusion_'..(nS==1 and 'RGB' or 'Flow')..'.png',confusion:render(nil,nil,38))
        end

        print('saving image')
        local img = torch.cat(imgCols,3)
        image.save(opt.outputPath..videoName..'_'..(nS==1 and 'RGB' or 'Flow')..'.png',img)
        print('image saved')
        videoDecoder.exit()
    end
    print('saving features')
    torch.save(opt.outputPath .. videoName ..'.t7',feat)
    print('features saved')
end
