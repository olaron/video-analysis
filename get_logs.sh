#!/usr/bin/env bash

while test $# -gt 0; do
    case "$1" in
        -h|--help)
            echo "options:"
            echo "-h, --help                show brief help"
            echo "-c, --checkpointPath=CHECKPOINT_PATH       specify the path containing the checkpoints folder from the RNN"
            echo "-o, --outputPath=OUTPUT_PATH       specify the output path where all the log would be stored"
            exit 0
            ;;
        -c)
            shift
            if test $# -gt 0; then
                export CHECKPOINT_PATH=$1
            else
                echo "no checkpoint path specified"
                exit 1
            fi
            shift
            ;;
        -o)
            shift
            if test $# -gt 0; then
                export OUTPUT_PATH=$1
            else
                echo "no output path specified"
                exit 1
            fi
            shift
            ;;
        *)
            break
            ;;
    esac
done

mkdir -p "${OUTPUT_PATH}"

for dir in "${CHECKPOINT_PATH}"/*/*/ ; do
    logname=$(basename "${dir}")
    cp "${dir}/log.txt" "${OUTPUT_PATH}/${logname}.txt"
done
