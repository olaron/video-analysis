require 'csvigo'

op = xlua.OptionParser('%prog [options]')

op:option{'-dA', '--dirAnotation', action='store', dest='dirAnnotation', help='annotation directory path', default=''}

opt, args = op:parse()

nameVideos=paths.dir(opt.dirAnnotation)
print(nameVideos)
table.sort(nameVideos) -- ascending order
table.remove(nameVideos,1) -- remove "."
table.remove(nameVideos,1) -- remove ".."

stats = {}

for i=1,#nameVideos do
    local videoName = nameVideos[i]
    local csvPath = paths.concat(opt.dirAnnotation,videoName)
    local csv = csvigo.load({path=csvPath,mode='large'})
    local previousPhase = 0
    local phaseLength = 0
    for c=1,#csv do
        local currentPhase = tonumber(csv[c][2])
        if currentPhase ~= previousPhase then
            if not stats[previousPhase] then
                stats[previousPhase] = {length = {}, next = {}}
            end
            table.insert(stats[previousPhase].length,phaseLength)
            table.insert(stats[previousPhase].next,currentPhase)
            phaseLength = 0
            previousPhase = currentPhase
        else
            phaseLength = phaseLength + 1
        end
    end
end

print(stats)