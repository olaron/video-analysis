#!/usr/bin/env bash

set -e


#Get the parameters
while test $# -gt 0; do
    case "$1" in
        -h|--help)
            echo "options:"
            echo "-h, --help                show brief help"
            echo "-i, --inputPath=INPUT_PATH       specify the path containing the Endovis folder"
            exit 0
            ;;
        -i)
            shift
            if test $# -gt 0; then
                export INPUT_PATH=$1
            else
                echo "no input path specified"
                exit 1
            fi
            shift
            ;;
        *)
            break
            ;;
    esac
done


INPUT_PATH=`readlink -f "${INPUT_PATH}"`

SCRIPT_DIR=$(dirname $(readlink -f $0))

cd "${SCRIPT_DIR}"

echo "${SCRIPT_DIR}"

echo "Training CNNs"
cd fb.resnet.torch

echo "Training RGB CNN"

th main.lua -nEpochs 30 -LR 0.001 -batchSize 100 -depth 34 -dropout 0.2 -opType Prokto -retrain "models/resnet-34.t7" -name EndoVis-RGB-100-0.001-34-Dropout_0.2-Prokto -data "${INPUT_PATH}/RGB-crop-frame-classes/" -save "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Prokto/" -resume "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Prokto/"
th main.lua -nEpochs 30 -LR 0.001 -batchSize 100 -depth 34 -dropout 0.2 -opType Rektum -retrain "models/resnet-34.t7" -name EndoVis-RGB-100-0.001-34-Dropout_0.2-Rektum -data "${INPUT_PATH}/RGB-crop-frame-classes/" -save "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Rektum/" -resume "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Rektum/"
th main.lua -nEpochs 30 -LR 0.001 -batchSize 100 -depth 34 -dropout 0.2 -opType Sigma -retrain "models/resnet-34.t7" -name EndoVis-RGB-100-0.001-34-Dropout_0.2-Sigma -data "${INPUT_PATH}/RGB-crop-frame-classes/" -save "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Sigma/" -resume "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Sigma/"

echo "Training Optical Flow CNN"

th main.lua -nEpochs 30 -LR 0.01 -batchSize 100 -depth 34 -dropout 0.2 -opType Prokto -retrain "models/resnet-34.t7" -dataset endovis-flow -name EndoVis-Flow-100-0.01-34-Dropout_0.2-Prokto -data "${INPUT_PATH}/Flow-crop-frame-classes/" -save "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Prokto/" -resume "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Prokto/"
th main.lua -nEpochs 30 -LR 0.01 -batchSize 100 -depth 34 -dropout 0.2 -opType Rektum -retrain "models/resnet-34.t7" -dataset endovis-flow -name EndoVis-Flow-100-0.01-34-Dropout_0.2-Rektum -data "${INPUT_PATH}/Flow-crop-frame-classes/" -save "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Rektum/" -resume "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Rektum/"
th main.lua -nEpochs 30 -LR 0.01 -batchSize 100 -depth 34 -dropout 0.2 -opType Sigma -retrain "models/resnet-34.t7" -dataset endovis-flow -name EndoVis-Flow-100-0.01-34-Dropout_0.2-Sigma -data "${INPUT_PATH}/Flow-crop-frame-classes/" -save "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Sigma/" -resume "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Sigma/"


cd "${SCRIPT_DIR}"

echo "Generating Features ..."

cd CNN-features

qlua twoStreams.lua -opType Prokto -sVP "${INPUT_PATH}/RGB-crop/" -tVP "${INPUT_PATH}/Flow-crop/" -sMP "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Prokto/model_best.t7" -tMP "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Prokto/model_best.t7" -dA "${INPUT_PATH}/PhaseAnnotation/" -oP "${INPUT_PATH}/Features-Dropout_0.2-Prokto/"
qlua twoStreams.lua -opType Rektum -sVP "${INPUT_PATH}/RGB-crop/" -tVP "${INPUT_PATH}/Flow-crop/" -sMP "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Rektum/model_best.t7" -tMP "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Rektum/model_best.t7" -dA "${INPUT_PATH}/PhaseAnnotation/" -oP "${INPUT_PATH}/Features-Dropout_0.2-Rektum/"
qlua twoStreams.lua -opType Sigma -sVP "${INPUT_PATH}/RGB-crop/" -tVP "${INPUT_PATH}/Flow-crop/" -sMP "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Sigma/model_best.t7" -tMP "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Sigma/model_best.t7" -dA "${INPUT_PATH}/PhaseAnnotation/" -oP "${INPUT_PATH}/Features-Dropout_0.2-Sigma/"

cd "${SCRIPT_DIR}"

echo "Training RNN"

cd RNN

th main.lua -pastalogName EndoVis-RNN-0.00001-Dropout_0.2-Prokto -opType Prokto -learningRate 0.00001 -featDir "${INPUT_PATH}/Features-Dropout_0.2-Prokto/" -logPath  "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-RNN-0.00001-Dropout_0.2-Prokto/"
th main.lua -pastalogName EndoVis-RNN-0.00001-Dropout_0.2-Rektum -opType Rektum -learningRate 0.00001 -featDir "${INPUT_PATH}/Features-Dropout_0.2-Rektum/" -logPath  "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-RNN-0.00001-Dropout_0.2-Rektum/"
th main.lua -pastalogName EndoVis-RNN-0.00001-Dropout_0.2-Sigma -opType Sigma -learningRate 0.00001 -featDir "${INPUT_PATH}/Features-Dropout_0.2-Sigma/" -logPath  "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-RNN-0.00001-Dropout_0.2-Sigma/"

echo "Generating csv files ..."

cd "${SCRIPT_DIR}"

cd Prediction

th RNN_prediction.lua -MP "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-RNN-0.0001-Dropout_0.2-Prokto-Dropout_0.8/log_EndoVis-RNN-0.0001-Dropout_0.2-Prokto-Dropout_0.8_{0}_{512}_0.0001_0_0.8/model_best.t7" -featDir "${INPUT_PATH}/Features-Dropout_0.2-Prokto/" -save "${INPUT_PATH}/PhasePredictions/" -opType Prokto -avg 10 -split val
th RNN_prediction.lua -MP "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-RNN-0.0001-Dropout_0.2-Rektum-Dropout_0.8/log_EndoVis-RNN-0.0001-Dropout_0.2-Rektum-Dropout_0.8_{0}_{512}_0.0001_0_0.8/model_best.t7" -featDir "${INPUT_PATH}/Features-Dropout_0.2-Rektum/" -save "${INPUT_PATH}/PhasePredictions/" -opType Rektum -avg 10 -split val
th RNN_prediction.lua -MP "${INPUT_PATH}/Checkpoints/checkpoints-EndoVis-RNN-0.0001-Dropout_0.2-Sigma-Dropout_0.8/log_EndoVis-RNN-0.0001-Dropout_0.2-Sigma-Dropout_0.8_{0}_{512}_0.0001_0_0.8/model_best.t7" -featDir "${INPUT_PATH}/Features-Dropout_0.2-Sigma/" -save "${INPUT_PATH}/PhasePredictions/" -opType Sigma -avg 10 -split val
