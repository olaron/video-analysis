#!/usr/bin/env bash

set -e

#Get the parameters
while test $# -gt 0; do
    case "$1" in
        -h|--help)
            echo "options:"
            echo "-h, --help                show brief help"
            echo "-i, --inputPath=INPUT_PATH       specify the path containing the testing videos"
            echo "-c, --checkpointsPath=CHECKPOINTS_PATH       specify the path containing the checkpoint folder"
            exit 0
            ;;
        -i)
            shift
            if test $# -gt 0; then
                export INPUT_PATH=$1
            else
                echo "no input path specified"
                exit 1
            fi
            shift
            ;;
        -c)
            shift
            if test $# -gt 0; then
                export CHECKPOINTS_PATH=$1
            else
                echo "no checkpoints path specified"
                exit 1
            fi
            shift
            ;;
        *)
            break
            ;;
    esac
done

INPUT_PATH=`readlink -f "${INPUT_PATH}"`
CHECKPOINTS_PATH=`readlink -f "${CHECKPOINTS_PATH}"`

SCRIPT_DIR=$(dirname $(readlink -f $0))

cd "${SCRIPT_DIR}"

echo "${SCRIPT_DIR}"

echo "Generating Features ..."

cd CNN-features
qlua twoStreams.lua -opType Prokto -tO -sVP "${INPUT_PATH}/RGB-crop/" -tVP "${INPUT_PATH}/Flow-crop/" -sMP "${CHECKPOINTS_PATH}/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Prokto/model_best.t7" -tMP "${CHECKPOINTS_PATH}/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Prokto/model_best.t7"  -oP "${INPUT_PATH}/Features-Dropout_0.2-Prokto/"
qlua twoStreams.lua -opType Rektum -tO -sVP "${INPUT_PATH}/RGB-crop/" -tVP "${INPUT_PATH}/Flow-crop/" -sMP "${CHECKPOINTS_PATH}/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Rektum/model_best.t7" -tMP "${CHECKPOINTS_PATH}/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Rektum/model_best.t7" -oP "${INPUT_PATH}/Features-Dropout_0.2-Rektum/"
qlua twoStreams.lua -opType Sigma -tO -sVP "${INPUT_PATH}/RGB-crop/" -tVP "${INPUT_PATH}/Flow-crop/" -sMP "${CHECKPOINTS_PATH}/checkpoints-EndoVis-RGB-100-0.001-34-Dropout_0.2-Sigma/model_best.t7" -tMP "${CHECKPOINTS_PATH}/checkpoints-EndoVis-Flow-100-0.01-34-Dropout_0.2-Sigma/model_best.t7" -oP "${INPUT_PATH}/Features-Dropout_0.2-Sigma/"

echo "Generating csv files ..."

cd "${SCRIPT_DIR}"

cd Prediction

th RNN_prediction.lua -MP "${CHECKPOINTS_PATH}/checkpoints-EndoVis-RNN-0.0001-Dropout_0.2-Prokto-Dropout_0.8/log_EndoVis-RNN-0.0001-Dropout_0.2-Prokto-Dropout_0.8_{0}_{512}_0.0001_0_0.8/model_best.t7" -featDir "${INPUT_PATH}/Features-Dropout_0.2-Prokto/" -save "${INPUT_PATH}/PhasePredictions/" -opType Prokto -avg 10 -split test
th RNN_prediction.lua -MP "${CHECKPOINTS_PATH}/checkpoints-EndoVis-RNN-0.0001-Dropout_0.2-Rektum-Dropout_0.8/log_EndoVis-RNN-0.0001-Dropout_0.2-Rektum-Dropout_0.8_{0}_{512}_0.0001_0_0.8/model_best.t7" -featDir "${INPUT_PATH}/Features-Dropout_0.2-Rektum/" -save "${INPUT_PATH}/PhasePredictions/" -opType Rektum -avg 10 -split test
th RNN_prediction.lua -MP "${CHECKPOINTS_PATH}/checkpoints-EndoVis-RNN-0.0001-Dropout_0.2-Sigma-Dropout_0.8/log_EndoVis-RNN-0.0001-Dropout_0.2-Sigma-Dropout_0.8_{0}_{512}_0.0001_0_0.8/model_best.t7" -featDir "${INPUT_PATH}/Features-Dropout_0.2-Sigma/" -save "${INPUT_PATH}/PhasePredictions/" -opType Sigma -avg 10 -split test
