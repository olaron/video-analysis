--
--  Copyright (c) 2016, Facebook, Inc.
--  All rights reserved.
--
--  This source code is licensed under the BSD-style license found in the
--  LICENSE file in the root directory of this source tree. An additional grant
--  of patent rights can be found in the PATENTS file in the same directory.
--
--  The training loop and learning rate schedule
--

local threads = require 'threads'
local pastathread = threads.Threads(1)
local pastatimer = torch.Timer()
local pastasending = false
local function pastalog_send(modelName, seriesName, value, step, url)
    if pastatimer:time().real > 5 then
        pastatimer:reset()
        if pastasending == false and pastathread.mainqueue.isempty ~= 1 then
            pastathread:dojob()
        end
        if pastathread:acceptsjob() then
            pastathread:addjob(function ()
                pastasending = true
                local pastalog = require 'pastalog'
                pastalog(modelName,seriesName,value,step,url)
                pastasending = false
            end)
        end

    end
    if pastathread:hasjob() then
        pastathread:synchronize()
    end
end

local optim = require 'optim'

local M = {}
local Trainer = torch.class('resnet.Trainer', M)

function Trainer:__init(model, criterion, opt, optimState)
    self.model = model
    self.criterion = criterion
    self.optimState = optimState or {
        learningRate = opt.LR,
        learningRateDecay = opt.LRD,
        momentum = opt.momentum,
        nesterov = true,
        dampening = 0.0,
        weightDecay = opt.weightDecay,
    }
    self.opt = opt
    self.params, self.gradParams = model:getParameters()
end

local globalTimer
function Trainer:train(epoch, dataloader)
    if not globalTimer then
        globalTimer = torch.Timer()
    end
    isTest = false
    -- Trains the model for a single epoch
    self.optimState.learningRate = self:learningRate(epoch)

    local timer = torch.Timer()
    local dataTimer = torch.Timer()
    local t = torch.Timer()

    local function feval()
        return self.criterion.output, self.gradParams
    end

    local trainSize = dataloader:size()
    local top1Sum, top3Sum, lossSum = 0.0, 0.0, 0.0
    local N = 0

    print('=> Training epoch # ' .. epoch)
    -- set the batch norm to training mode
    self.model:training()
    for n, sample in dataloader:run() do
        local dataTime = dataTimer:time().real

        -- Copy input and target to the GPU
        self:copyInputs(sample)
        local output = self.model:forward(self.input):float()
        local batchSize = output:size(1)
        local loss = self.criterion:forward(self.model.output, self.target)

        self.model:zeroGradParameters()
        self.criterion:backward(self.model.output, self.target)
        self.model:backward(self.input, self.criterion.gradInput)

        optim.sgd(feval, self.params, self.optimState)

        local top1, top3 = self:computeScore(output, sample.target, 1)
        top1Sum = top1Sum + top1 * batchSize
        top3Sum = top3Sum + top3 * batchSize
        lossSum = lossSum + loss * batchSize
        N = N + batchSize

        local timeSpent = t:time().real
        local eta = ((timeSpent / n) * (trainSize - n))/60
        
        local globalTime = globalTimer:time().real
        local epochProgress = (epoch-1) + n/trainSize
        local avgTimeEpoch = (globalTime/epochProgress)/60

        print(('Epoch: [%d][%d/%d]  Time %.3f  Data %.3f  Err %1.4f  top1 %7.3f  top3 %7.3f  ETA %9.2fm  AvgTimeEpoch %9.2fm'):
        format(epoch, n, trainSize, timer:time().real, dataTime, loss, top1, top3, eta, avgTimeEpoch))

        --TODO be able to change the url via opt
        pastalog_send(self.opt.name or self.opt.dataset,today_date,top1, (epoch-1)+ n/trainSize,'http://10.100.222.155:8120/data')

        -- check that the storage didn't get changed due to an unfortunate getParameters call
        assert(self.params:storage() == self.model:parameters()[1]:storage())

        timer:reset()
        dataTimer:reset()
    end

    return top1Sum / N, top3Sum / N, lossSum / N
end

function Trainer:test(epoch, dataloader)
    isTest = true
    -- Computes the top-1 and top-5 err on the validation set

    local t = torch.Timer()
    local timer = torch.Timer()
    local dataTimer = torch.Timer()
    local size = dataloader:size()

    local nCrops = self.opt.tenCrop and 10 or 1
    local top1Sum, top3Sum, top1Sum_nb, top3Sum_nb = 0.0, 0.0, 0.0, 0.0
    local N = 0
    local N_nb = 0

    self.model:evaluate()
    for n, sample in dataloader:run() do
        local dataTime = dataTimer:time().real

        -- Copy input and target to the GPU
        self:copyInputs(sample)

        local output = self.model:forward(self.input):float()
        local batchSize = output:size(1) / nCrops
        local loss = self.criterion:forward(self.model.output, self.target)

        local top1, top3, top1_nb, top3_nb, batchSize_nb = self:computeScore(output, sample.target, nCrops)
        top1Sum = top1Sum + top1 * batchSize
        top3Sum = top3Sum + top3 * batchSize
        N = N + batchSize

        top1Sum_nb = top1Sum_nb + top1_nb * batchSize_nb
        top3Sum_nb = top3Sum_nb + top3_nb * batchSize_nb
        N_nb = N_nb + batchSize_nb
        
        local timeSpent = t:time().real
        local eta = ((timeSpent / n) * (size - n))/60

        print((' | Test: [%d][%d/%d]  Time %.3f  Data %.3f  top1 %7.3f (%7.3f) (%7.3f)  top3 %7.3f (%7.3f) (%7.3f)  ETA %9.2fm'):
        format(epoch, n, size, timer:time().real, dataTime, top1, top1Sum / N, top1Sum_nb / N_nb , top3, top3Sum / N, top3Sum_nb / N_nb , eta))

        timer:reset()
        dataTimer:reset()
    end
    self.model:training()

    print((' * Finished epoch # %d     top1: %7.3f (%7.3f)  top3: %7.3f (%7.3f)\n'):format(epoch, top1Sum / N,top1Sum_nb / N_nb, top3Sum / N,top3Sum_nb / N_nb))

    return top1Sum / N, top3Sum / N, top1Sum_nb / N_nb, top3Sum_nb / N_nb
end

function Trainer:computeScore(output, target, nCrops)

    if nCrops > 1 then
        -- Sum over crops
        output = output:view(output:size(1) / nCrops, nCrops, output:size(2))
        --:exp():sum(2):squeeze(2)
    end

    -- Coputes the top1 and top3 error rate
    local batchSize = output:size(1)


    local _, predictions = output:float():topk(3, 2, true, true) -- descending

    local targets = target:long():view(batchSize, 1):expandAs(predictions)




    -- Find which predictions match the target
    local correct = predictions:eq(targets)

    --print(prediction_nb)

    -- Top-1 score
    local top1 = 1.0 - (correct:narrow(2, 1, 1):sum() / batchSize)

    -- Top-5 score, if there are at least 5 classes
    local len = math.min(3, correct:size(2))
    local top3 = 1.0 - (correct:narrow(2, 1, len):sum() / batchSize)

    local top1_nb, top3_nb = 0.0, 0.0
    local batchSize_nb = targets:ne(10):sum()/3
    if batchSize_nb >= 1 then
        local prediction_nb = predictions[targets:ne(10)]:reshape(batchSize_nb,3)
        local correct_nb = prediction_nb:eq(targets[targets:ne(10)])

        -- Top-1 score
        top1_nb = 1.0 - (correct_nb:narrow(2, 1, 1):sum() / batchSize_nb)

        -- Top-5 score, if there are at least 5 classes
        local len_nb = math.min(3, correct_nb:size(2))
        top3_nb = 1.0 - (correct_nb:narrow(2, 1, len_nb):sum() / batchSize_nb)
    end
    return top1 * 100, top3 * 100, top1_nb * 100, top3_nb * 100, batchSize_nb
end

local function getCudaTensorType(tensorType)
    if tensorType == 'torch.CudaHalfTensor' then
        return cutorch.createCudaHostHalfTensor()
    elseif tensorType == 'torch.CudaDoubleTensor' then
        return cutorch.createCudaHostDoubleTensor()
    else
        return cutorch.createCudaHostTensor()
    end
end

function Trainer:copyInputs(sample)
    -- Copies the input to a CUDA tensor, if using 1 GPU, or to pinned memory,
    -- if using DataParallelTable. The target is always copied to a CUDA tensor
    self.input = self.input or (self.opt.nGPU == 1
            and torch[self.opt.tensorType:match('torch.(%a+)')]()
            or getCudaTensorType(self.opt.tensorType))
    self.target = self.target or (torch.CudaLongTensor and torch.CudaLongTensor())
    self.input:resize(sample.input:size()):copy(sample.input)
    self.target:resize(sample.target:size()):copy(sample.target)
end

function Trainer:learningRate(epoch)
    -- Training schedule
    local decay = 0
    if self.opt.dataset == 'endovis' then
        decay = math.floor((epoch - 1) / 30)
    elseif self.opt.dataset == 'endovis-flow' then
        decay = math.floor((epoch - 1) / 30)
    elseif self.opt.dataset == 'imagenet' then
        decay = math.floor((epoch - 1) / 30)
    elseif self.opt.dataset == 'cifar10' then
        decay = epoch >= 122 and 2 or epoch >= 81 and 1 or 0
    elseif self.opt.dataset == 'cifar100' then
        decay = epoch >= 122 and 2 or epoch >= 81 and 1 or 0
    end
    return self.opt.LR * math.pow(0.1, decay)
end

return M.Trainer
