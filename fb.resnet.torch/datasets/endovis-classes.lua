return {
    "Preparation and orientation at abdomen",
    "Dissection of lymphnodes and blood vessels",
    "Retroperitoneal preparation to lower pancreatic border",
    "Retroperitoneal preparation of duodenum and pancreatic head",
    "Mobilizing the sigmoid and the descending colon",
    "Mobilizing the spenic flexure",
    "Mobilizing the tranverse colon",
    "Mobilizing the ascending colon",
    "Dissection and resection of rectum",
    "Preparing the anastomosis extraabdominally",
    "Preparing the anastomosis intraabdominally",
    "Placing stoma",
    "Finishing the operation",
    "NOOP"
}
