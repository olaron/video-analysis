local paths = require 'paths'
local t = require 'datasets/transforms'
local ffi = require 'ffi'
require 'torchzlib'

local M = {}
local EndoVisFlowDataset = torch.class('resnet.EndoVisFlowDataset', M)

function EndoVisFlowDataset:__init(imageInfo, opt, split)
    self.imageInfo = imageInfo[split]
    self.opt = opt
    self.split = split
    self.dir = opt.data
    assert(paths.dirp(self.dir), 'directory does not exist: ' .. self.dir)
end

function EndoVisFlowDataset:get(i)
    local path = ffi.string(self.imageInfo.imagePath[i]:data())

    local image = self:_loadImage(paths.concat(self.dir, path))
    local class = self.imageInfo.imageClass[i]

    return {
        input = image,
        target = class,
    }
end

function EndoVisFlowDataset:_loadImage(path)
    local compressed = torch.load(path)
    return compressed:decompress():double():div(255)
end

function EndoVisFlowDataset:size()
    return self.imageInfo.imageClass:size(1)
end

function EndoVisFlowDataset:preprocess()
    if self.split == 'train' then
        return t.Compose {
            t.HorizontalFlip(0.5),
            t.VerticalFlip(0.5),
            t.Rotation90()
        }
    elseif self.split == 'val' then
        --local Crop = self.opt.tenCrop and t.TenCrop or t.CenterCrop
        return t.Compose {}
    else
        error('invalid split: ' .. self.split)
    end
end

return M.EndoVisFlowDataset
