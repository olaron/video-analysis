local image = require 'image'
local paths = require 'paths'
local t = require 'datasets/transforms'
local ffi = require 'ffi'

local M = {}
local EndovisDataset = torch.class('resnet.EndovisDataset', M)

function EndovisDataset:__init(imageInfo, opt, split)
   self.imageInfo = imageInfo[split]
   self.opt = opt
   self.split = split
   self.dir = opt.data
   assert(paths.dirp(self.dir), 'directory does not exist: ' .. self.dir)
end

function EndovisDataset:get(i)
   local path = ffi.string(self.imageInfo.imagePath[i]:data())

   local image = self:_loadImage(paths.concat(self.dir, path))
   local class = self.imageInfo.imageClass[i]

   return {
      input = image,
      target = class,
   }
end

function EndovisDataset:_loadImage(path)
   local ok, input = pcall(function()
      return image.load(path, 3, 'float')
   end)

   -- Sometimes image.load fails because the file extension does not match the
   -- image format. In that case, use image.decompress on a ByteTensor.
   if not ok then
      local f = io.open(path, 'r')
      assert(f, 'Error reading: ' .. tostring(path))
      local data = f:read('*a')
      f:close()

      local b = torch.ByteTensor(string.len(data))
      ffi.copy(b:data(), data, b:size(1))

      input = image.decompress(b, 3, 'float')
   end

   return input
end

function EndovisDataset:size()
   return self.imageInfo.imageClass:size(1)
end

function EndovisDataset:preprocess()
   if self.split == 'train' then
      return t.Compose{
          t.Hue(0.04),
          t.ColorJitter({
              brightness = 0.4,
              contrast = 0.4,
              saturation = 0.4,
          }),
          t.HorizontalFlip(0.5),
          t.VerticalFlip(0.5),
          t.Rotation90(),
          --t.RandomSizedCrop(224),
          t.RandomBlur(2)
      }
   elseif self.split == 'val' then
      --local Crop = self.opt.tenCrop and t.TenCrop or t.CenterCrop
       return t.Compose{}
   else
      error('invalid split: ' .. self.split)
   end
end

return M.EndovisDataset
