--
--  Copyright (c) 2016, Facebook, Inc.
--  All rights reserved.
--
--  This source code is licensed under the BSD-style license found in the
--  LICENSE file in the root directory of this source tree. An additional grant
--  of patent rights can be found in the PATENTS file in the same directory.
--
--  Script to compute list of ImageNet filenames and classes
--
--  This generates a file gen/imagenet.t7 which contains the list of all
--  ImageNet training and validation images and their classes. This script also
--  works for other datasets arragned with the same layout.
--

local op_type_classes = {}--   0    1    2     3    4    5    6    7    8    9   10   11   12    13
op_type_classes['Prokto'] = {true,true,false,true,true,true,true,true,true,true,true,true,true,false}
op_type_classes['Sigma'] =  {true,true,true,false,true,true,true,false,true,true,true,false,true,false}
op_type_classes['Rektum'] = {true,true,true,false,true,true,true,false,true,true,true,true,true,false}

local sys = require 'sys'
local ffi = require 'ffi'
local splits = require 'datasets/endovis-splits'
local classList = require 'datasets/endovis-classes'

local M = {}

local function findImages(dir,opType)
    ----------------------------------------------------------------------
    -- Options for the GNU and BSD find command
    local extensionList = { 'jpg', 'png', 'jpeg', 'JPG', 'PNG', 'JPEG', 'ppm', 'PPM', 'bmp', 'BMP' }
    local findOptions = ' -iname "*.' .. extensionList[1] .. '"'
    for i = 2, #extensionList do
        findOptions = findOptions .. ' -o -iname "*.' .. extensionList[i] .. '"'
    end

    -- Find all the images using the find command
    local f = io.popen('find -L ' .. dir ..findOptions)

    local maxLength = -1
    local imagePaths = {}
    local imageClasses = {}

    -- Generate a list of all the images and their class
    while true do
        local line = f:read('*line')
        if not line then break end

        local className = paths.basename(paths.dirname(line))
        local videoName = paths.basename(paths.dirname(paths.dirname(line)))
        local filename = paths.basename(line)
        local frameNumber = tonumber(filename:match("_(%d+).png"))

        if frameNumber % 25 == 0 then

            local path = videoName .. '/' .. className .. '/' .. filename

            local classId = tonumber(className)+1

            assert(classId, 'class unknown: ' .. className)
            if op_type_classes[opType][classId] then
                table.insert(imagePaths, path)
                table.insert(imageClasses, classId)

                maxLength = math.max(maxLength, #path + 1)
            end
        end
    end

    f:close()

    -- Convert the generated list to a tensor for faster loading
    local nImages = #imagePaths
    local imagePath = torch.CharTensor(nImages, maxLength):zero()
    for i, path in ipairs(imagePaths) do
        ffi.copy(imagePath[i]:data(), path)
    end

    local imageClass = torch.LongTensor(imageClasses)
    return imagePath, imageClass
end

function M.exec(opt, cacheFile)
    -- find the image path names

    local trainDir = ''
    for i, vid in ipairs(splits[opt.opType].train) do
        local dir = paths.concat(opt.data, vid)
        assert(paths.dirp(dir),'Error: directory not found: ' .. dir)
        trainDir = trainDir .. '"' .. dir ..'" '
    end
    local valDir = ''
    for i, vid in ipairs(splits[opt.opType].test) do
        local dir = paths.concat(opt.data, vid)
        assert(paths.dirp(dir),'Error: directory not found: ' .. dir)
        valDir = valDir .. '"' .. dir ..'" '
    end

    print(" | finding all validation images")
    local valImagePath, valImageClass = findImages(valDir,opt.opType)

    print(" | finding all training images")
    local trainImagePath, trainImageClass = findImages(trainDir,opt.opType)

    local info = {
        basedir = opt.data,
        classList = classList,
        train = {
            imagePath = trainImagePath,
            imageClass = trainImageClass,
        },
        val = {
            imagePath = valImagePath,
            imageClass = valImageClass,
        },
    }

    print(" | saving list of images to " .. cacheFile)
    torch.save(cacheFile, info)
    return info
end

return M
