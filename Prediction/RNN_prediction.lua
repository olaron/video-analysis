
require 'torch'
require 'nn'
require 'cudnn'
require 'cunn'
require 'cutorch'
require 'rnn'
require 'csvigo'
require 'image'
optim = require 'optim'
splits = require 'endovis-splits'

local op_type_classes = {} -- Phases to ignore
op_type_classes['Prokto'] = {2+1,13+1}
op_type_classes['Sigma'] =  {3+1,7+1,11+1,13+1}
op_type_classes['Rektum'] = {3+1,7+1,13+1}

op = xlua.OptionParser('%prog [options]')

op:option { '-MP', '--RnnModelPath', action = 'store', dest = 'modelPath', help = 'RNN model path', default = 'none' }
op:option { '-AP', '--AnnotationPath', action = 'store', dest = 'annotationPath', help = 'Annotation path', default = 'none' }
op:option { '-featDir', '--featDir', action = 'store', dest = 'featDir', help = 'Feature directory ', default = 'none' }
op:option { '-save', '--save', action = 'store', dest = 'save', help = 'Directory to save the csv files ', default = 'none' }
op:option { '-opType', '--opType', action = 'store', dest = 'opType', help = 'Type of operation (Rektum | Sigma | Prokto)', default = 'none' }
op:option { '-avg', '--averageWindow', action = 'store', dest = 'avg', help = 'Length of the averaging window', default = '1' }
op:option { '-split', '--split', action = 'store', dest = 'split', help = 'Split to be using (all | train | val | test)', default = 'test' }

opt, args = op:parse()

opt.avg = tonumber(opt.avg)-1

if not op_type_classes[opt.opType] then
    error('--opType not specified or unknown (Rektum | Sigma | Prokto)')
end

if not paths.dirp(opt.save) then
    paths.mkdir(opt.save)
end

local imgCols
function graph_init()
    imgCols = {}
end

function graph_add(pred,label)
    local labelCol = torch.zeros(14)
    if label then
        labelCol[label+1]=1
    end
    local outputPred = torch.zeros(14)
    outputPred[pred]=1
    local col = torch.cat({outputPred:float(),torch.zeros(14):float(),labelCol:float()},2)
    imgCols[#imgCols+1]= col:transpose(1,2)
end

function graph_save(path)
    local img = torch.cat(imgCols,3)
    image.save(path,img)
end

function low_high (myTable)
    local lowest, highest

    for k,_ in pairs(myTable) do
        if type(k) == "number" and k % 1 == 0 and k > 0 then -- Assuming mixed (possibly non-integer) keys
            if lowest then
                lowest = math.min(lowest, k)
                highest = math.max(highest, k)
            else
                lowest, highest = k, k
            end
        end
    end

    return lowest or 0, highest or 0 -- "or 0" in case there were no indices
end

local testFeats = {}

videos = splits[opt.opType][opt.split]
local frameInterval = 25
for _,v in ipairs(videos) do
    local featsFile = torch.load(paths.concat(opt.featDir,v..'.t7'))
    local feats = {}
    print(v)
    local low_i,_ = low_high(featsFile.Flow.frame)
    for i,w in pairs(featsFile.Flow.frame) do
        local feat = w
        feat.output = nil
        feat.frameNumber = i
        feat.feature = torch.cat(featsFile.RGB.frame[i].feature,featsFile.Flow.frame[i].feature,2):squeeze()
        feats[i/frameInterval-low_i/frameInterval+1] = feat
    end
    testFeats[v] = {feats,featsFile.length}
    collectgarbage()
end

local model = torch.load(opt.modelPath):cuda()


for videoName,frames in pairs(testFeats) do
    local outputs = {}
    local csvPath = paths.concat(opt.annotationPath,videoName..'.csv')
    local csv
    if paths.filep(csvPath) then
        csv = csvigo.load({path=csvPath,mode='large'})
    else
        print('Annotation file: '..csvPath..' not found, skipping...')
    end
    graph_init()
    local annotation = {}

    for i=1,225 do
        table.insert(annotation,{i-1,0})
    end
    local _,val
    for i=1,#frames[1] do
        local label
        if csv then label = tonumber(csv[frames[1][i].frameNumber+1][2]) end
        local input = torch.reshape(frames[1][i].feature,1,1024):cuda()
        local output = model:forward(input):float()[1]
        for _,v in pairs(op_type_classes[opt.opType]) do
            output[v] = -math.huge
        end
        _,val = output:max(1)
        val = val[1]
        outputs[#outputs+1] = val

        -- Average window
        local counter = torch.zeros(14)
        for i=#outputs,#outputs-opt.avg,-1 do
            if i <= 0 then break end
            counter[outputs[i]] = counter[outputs[i]] + 1
        end
        _,val = counter:max(1)
        val = val[1]

        graph_add(val,label)
        for k=0,24 do
            if frames[1][i].frameNumber+k > frames[2]-1 then break end
            table.insert(annotation,{frames[1][i].frameNumber+k,val-1})
        end
    end
    for i=frames[1][#frames[1]].frameNumber+25,frames[2]-1 do
        table.insert(annotation,{i,val-1})
    end
    csvigo.save({path=paths.concat(opt.save, videoName .. '_res.csv'),data=annotation})
    graph_save(paths.concat(opt.save, videoName .. '.png'))
end

