require 'torch'
require 'image'
require 'csvigo'
require 'torchzlib'

local videoDecoder = assert(require("libvideo_decoder"))

function alphanumsort(o)
    local function padnum(d) local dec, n = string.match(d, "(%.?)0*(.+)")
        return #dec > 0 and ("%.12f"):format(d) or ("%s%03d%s"):format(dec, #n, n) end
    table.sort(o, function(a,b)
        return tostring(a):gsub("%.?%d+",padnum)..("%3d"):format(#b)
                < tostring(b):gsub("%.?%d+",padnum)..("%3d"):format(#a) end)
    return o
end

op = xlua.OptionParser('%prog [options]')

op:option{'-dS', '--dirSource', action='store', dest='dirSource', help='source directory path', default='' }
op:option{'-v', '--video', action='store_true', dest='video', help='extract frames from videos', default=false}
op:option{'-fps', '--framePerSec', action='store', dest='fps', help='number of frames per second converted', default='1'}
op:option{'-dO', '--dirOutput', action='store', dest='dirOutput', help='output directory path', default='' }
op:option{'-dA', '--dirAnotation', action='store', dest='dirAnnotation', help='annotation directory path', default=''}
op:option{'-f', '--flow', action='store_true', dest='flow', help='activate optical flow stacking', default=false}
op:option{'-nS', '--nStack', action='store', dest='nStack', help='number of stacked images per frame', default= '10' }
op:option{'-ov', '--overlap', action='store_true', dest='overlap', help='save stacks of frames that overlap', default= true }


opt, args = op:parse()

opt.nStack = tonumber(opt.nStack)
opt.fps = tonumber(opt.fps)
nameVideos=paths.dir(opt.dirSource)
nameVideos = alphanumsort(nameVideos) -- ascending order
table.remove(nameVideos,1) -- remove "."
table.remove(nameVideos,1) -- remove ".."

local message = ''
function save_frame(frame,queue,frameNumber,frameCount,csv,videoName,timer)
    local timeSpent = timer:time().real
    local eta = (timeSpent / (frameNumber+1)) * ((frameCount) - (frameNumber+1))

    io.write(string.rep('\b',#message))
    message = string.format('[%s] Frame: %7d/%d | ETA: %9.2fs',videoName,frameNumber,frameCount-1,eta)
    io.write(message)
    io.flush()

    local classNumber = csv[frameNumber+1][2]
    local frameName = videoName .. '_' .. frameNumber .. '.png'
    local outPath = paths.concat(opt.dirOutput,videoName,classNumber,frameName)

    local outDir = paths.dirname(outPath)
    if not paths.dirp(outDir) then
        if not paths.mkdir(outDir) then
            error('Could not create directory: ' .. outDir)
        else
            print('Directory '.. outDir .. ' created')
        end
    end

    if opt.flow then
        if opt.nStack == 0 then
            image.save(outPath,frame)
        else
            frame = frame[{{2,3}}]
            table.insert(queue,#queue+1,frame)
            if #queue == opt.nStack then
                local stack = torch.cat(queue,1)
                local compressed = torch.CompressedTensor(stack,2,false)
                outPath = string.sub(outPath,1,-4) .. 't7'
                torch.save(outPath,compressed)
                if opt.overlap then
                    table.remove(queue,1)
                else
                    for k,v in pairs(queue) do queue[k] = nil end
                end
            end
        end
    else
        if type(frame)=='string' then
            os.execute('mv "'..frame..'" "'..outPath..'"')
        else
            image.save(outPath,frame)
        end
    end

end


for nv = 1, #nameVideos do
    local videoName = nameVideos[nv]
    local videoPath = paths.concat(opt.dirSource,videoName)
    videoName = paths.basename(videoName,paths.extname(videoPath))
    print('Processing '..videoName..'...')

    local csvPath = paths.concat(opt.dirAnnotation,videoName..'.csv')
    local csv = csvigo.load({path=csvPath,mode='large'})

    local t = torch.Timer()

    local queue = {}

    if not opt.video then

        local frameNames = paths.dir(paths.concat(opt.dirSource,videoName))
        frameNames = alphanumsort(frameNames) -- ascending order
        table.remove(frameNames,1) -- remove "."
        table.remove(frameNames,1) -- remove ".."

        for nf = 1, #frameNames do
            local frameName = frameNames[nf]
            local inPath = paths.concat(opt.dirSource,videoName,frameName)
            --local frame = image.load(inPath,3,'byte')

            if string.match(frameName,videoName..'_'..(nf-1)..'.png') then
                save_frame(inPath,queue,nf-1,#frameNames,csv,videoName,t)
            else
                error(frameName .. ' seems not to correspond to the '..nf..'th frame of the video')
            end

        end
    else

        local status, height, width, length, fps = videoDecoder.init(videoPath)
        local frame = torch.ByteTensor(3, height, width)
        local frameNumber = 0
        while videoDecoder.frame_rgb(frame) do
            if (frameNumber % fps) % math.ceil(fps / opt.fps) == 0 then 
                save_frame(frame,queue,frameNumber,length,csv,videoName,t)
            end
            frameNumber = frameNumber + 1
        end
    end
end
